Run
===

You must have python>=3.10 installed on your machine.
You must have git installed as well and own a gitlab account.
With that follow the instructions below to run & use the app.

    $ git clone git@gitlab.com:nnaabiill/codesign-test.git
    $ cd codesign-test/
    $ pip install -r requirements.txt
    $ python manage.py makemigrations
    $ python manage.py migrate
    $ python manage.py runserver


 ***Now Go to http://localhost:8000/api-doc/ to get the api documentation***
 
 Run the following command if to both create new users(superusers) and access the admin dashboard.
 As I haven't written any registration api, you must need some users to call authenticated api endpoint.
    
    $ python manage.py createsuperuser
    
Provide your  username, email[Optional], and password. Your superuser will be created. Then run the server:

    $ python manage.py runserver
    
Now you can access admin panel by login to http://localhost:8000/admin/


Public Apis
===

+ http://localhost:8000/login/ Returns a login token for authenticating user
+ http://localhost:8000/list-public-color-palette/ List all published color palettes

Authenticated Apis:
===
For clients to authenticate(for authenticated api endpoint), the token key should be included in the Authorization HTTP header.
The key should be prefixed by the string literal "Token", with whitespace separating the two strings. For example:
Authorization: Token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b
Postman is recommended to use the authenticated apis. Go to this link ***https://www.postman.com/*** to get the platform

+ http://localhost:8000/create-color-palette/ Creates Color Palette for an authenticated user
+ http://localhost:8000/list-user-color-palette/ List all color palettes created by a user
+ http://localhost:8000/retrieve-publish-color-palette/<int:pk>/ Retrieve a color palette and set published=True or False
+ http://localhost:8000/search-color-palette-by-color/ Search color palettes by a color code
+ http://localhost:8000/list-create-user-color-favourite-palette/  List a user's set favourite color palettes
    Creates a user's favourite color palettes
