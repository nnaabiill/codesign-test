from django.db import models
from django.conf import settings


class TimeStamp(models.Model):
    created_at = models.DateField(auto_now_add=True)

    class Meta:
        abstract = True


class Color(TimeStamp):
    code = models.CharField(max_length=6, help_text='color hex code')

    class Meta:
        ordering = ['-created_at']

    def __str__(self) -> str:
        return self.code
    

class Palette(TimeStamp):
    creator = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='palletes',
        on_delete=models.CASCADE
    )
    title = models.CharField(max_length=150)
    dominant_colors = models.ManyToManyField(
        Color,
        related_name='dominant_colors',
        blank=False
    )
    accent_colors = models.ManyToManyField(
        Color,
        related_name='accent_colors',
        blank=False
    )
    published = models.BooleanField(default=False)

    class Meta:
        ordering = ['-created_at']

    def __str__(self) -> str:
        return self.title
    

class FavouritePalette(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='favourites',
        on_delete=models.CASCADE
    )
    palette = models.ForeignKey(
        Palette,
        on_delete=models.CASCADE
    )
    favourite = models.BooleanField(default=True)

    class Meta:
        ordering = ['-id']

    def __str__(self) -> str:
        return f"{self.palette.title} is {self.user.username}s favourite"


