from django.contrib import admin

from .models import (
    Color,
    Palette,
    FavouritePalette
)


admin.site.register(Color)
admin.site.register(Palette)
admin.site.register(FavouritePalette)

