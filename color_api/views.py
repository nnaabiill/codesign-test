from itertools import chain

from django.shortcuts import get_object_or_404

from rest_framework import status
from rest_framework import generics
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response

from .permissions import (
    IsCreator,
)

from .models import (
    Color,
    Palette,
    FavouritePalette
)
from .serializer import (
    PaleteSerializer,
    CreatePalleteSerializer,
    FavouritePaletteSerializer
)


class CreateColorPallete(generics.CreateAPIView):
    """
    Creates Color Palette for an authenticated user
    """
    serializer_class = CreatePalleteSerializer
    permission_classes = [permissions.IsAuthenticated, ]

    def perform_create(self, serializer):
        print(serializer.data)
        colors = zip(serializer.data.get('dominant_colors'), serializer.data.get('accent_colors'))
        dominant_colors = []
        accent_colors = []

        for dominant_color, accent_color in colors:
            dominant_colors.append(Color.objects.get_or_create(code=dominant_color)[0])
            accent_colors.append(Color.objects.get_or_create(code=accent_color)[0])
        
        palette = Palette.objects.create(
            creator=self.request.user,
            title=serializer.data.get('title'),
            published=serializer.data.get('published')
        )
        palette.dominant_colors.add(*dominant_colors)
        palette.accent_colors.add(*accent_colors)

        return palette

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    

class ListUserColorPatette(generics.ListAPIView):
    """List all color palettes created by a user"""
    model = Palette
    serializer_class = PaleteSerializer
    permission_classes = [permissions.IsAuthenticated, ]

    def get_queryset(self):
        return Palette.objects.filter(creator=self.request.user)
    

class RetrievePublishColorPalette(generics.RetrieveUpdateAPIView):
    """Retrieve a color palette and publishe or unpublish the palette"""
    model = Palette
    permission_classes = [permissions.IsAuthenticated, IsCreator, ]
    serializer_class = PaleteSerializer
    queryset = Palette.objects.all()

    def update(self, request, *args, **kwargs):
        return super().update(request, partial=True, *args, **kwargs)
    
    
class ListPublicColorPalette(generics.ListAPIView):
    """List all published color palettes"""
    model = Palette
    serializer_class = PaleteSerializer
    permission_classes = [permissions.AllowAny, ]
    queryset = Palette.objects.filter(published=True)


class SearchColorPaletteByColor(APIView):
    """Search color palettes by a color code"""
    def post(self, *args, **kwargs):
        color = get_object_or_404(Color, code=self.request.data.get('code', ""))

        if color:
            as_dominant = set(color.dominant_colors.all())
            as_accent = set(color.accent_colors.all())
            queryset = chain(as_dominant, as_accent)

        serializer = PaleteSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ListCreateUserFavouriteColorPalette(generics.ListCreateAPIView):
    """
    List a user's set favourite color palettes
    Creates a user's favourite color palettes
    """
    model = FavouritePalette
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = FavouritePaletteSerializer
    queryset = FavouritePalette.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)

    




