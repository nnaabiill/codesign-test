from django.urls import path

from .views import (
    
    CreateColorPallete,
    ListUserColorPatette,
    ListPublicColorPalette,
    SearchColorPaletteByColor,
    RetrievePublishColorPalette,
    ListCreateUserFavouriteColorPalette,
)


urlpatterns = [
    path(
        'create-color-palette/',
        CreateColorPallete.as_view(),
        name='create-color-palette'
    ),
    path(
        'list-user-color-palette/',
        ListUserColorPatette.as_view(),
        name='list-user-color-palette'
    ),
    path(
        'list-public-color-palette/',
        ListPublicColorPalette.as_view(),
        name='list-public-color-palette'
    ),
    path(
        'search-color-palette-by-color/',
        SearchColorPaletteByColor.as_view(),
        name='search-color-palette-by-color'
    ),
    path(
        'retrieve-publish-color-palette/<int:pk>/',
        RetrievePublishColorPalette.as_view(),
        name='retrieve-publish-color-paletter'
    ),
    path(
        'list-create-user-color-favourite-palette/',
        ListCreateUserFavouriteColorPalette.as_view(),
        name='list-create-user-favourite-color-palette'
    ),
]