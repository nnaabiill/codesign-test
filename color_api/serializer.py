from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model

from rest_framework import serializers

from .models import (
    Color,
    Palette,
    FavouritePalette
)
from user_account.serializers import UserSerializer


User = get_user_model()


class ColorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Color
        fields = ("id", "code")


class CreatePalleteSerializer(serializers.Serializer):
    title = serializers.CharField()
    dominant_colors = serializers.ListField(min_length=1, max_length=2)
    accent_colors = serializers.ListField(min_length=2, max_length=4)
    published = serializers.BooleanField(default=False)


class PaleteSerializer(serializers.ModelSerializer):
    dominant_colors = ColorSerializer(many=True, read_only=True)
    accent_colors = ColorSerializer(many=True, read_only=True)

    class Meta:
        model = Palette
        fields = (
            "id",
            "creator",
            "title",
            "dominant_colors",
            "accent_colors",
            "published"
        )


class FavouritePaletteSerializer(serializers.ModelSerializer):
    class Meta:
        model = FavouritePalette
        fields = "__all__"

    def validate_palette(self, palette):
        if not palette.published:
            raise serializers.ValidationError("The palette is not published yet")
        return palette
    
    def create(self, validated_data):
        user = get_object_or_404(User, id=validated_data.get('user').id)
        palette = get_object_or_404(Palette, id=validated_data.get('palette').id)
        favourite_palette, _ = FavouritePalette.objects.get_or_create(
            user=user,
            palette=palette
        )
        favourite_palette.favourite = True
        favourite_palette.save()

        return favourite_palette

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['user'] = UserSerializer(instance.user).data
        response['palette'] = PaleteSerializer(instance.palette).data
        return response


