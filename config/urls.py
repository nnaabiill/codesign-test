from django.contrib import admin
from django.urls import path, include

from drf_yasg import openapi
from drf_yasg.views import get_schema_view

from rest_framework import permissions


schema_view = get_schema_view(
   openapi.Info(
      title="Color Palette Api",
      default_version='v1',
      description="Codesign Test",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="moinul.hossain.in2019@gmail.com"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-doc/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('', include('user_account.urls')),
    path('', include('color_api.urls')),
]
